#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve, collatz_cycle_length, range_split

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "5 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  5)
        self.assertEqual(j, 200)

    def test_read_2(self):
        s = "2 11\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  2)
        self.assertEqual(j, 11)

    def test_read_3(self):
        s = "100 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  100)
        self.assertEqual(j, 10)

    def test_read_4(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertNotEqual(i, 10)
        self.assertNotEqual(j, 1)

    # ----
    # cycle_length
    # ----

    def test_cycle_length_1(self):
        c = collatz_cycle_length(1)
        self.assertEqual(c, 1)

    def test_cycle_length_5(self):
        c = collatz_cycle_length(5)
        self.assertEqual(c, 6)

    def test_cycle_length_10(self):
        c = collatz_cycle_length(10)
        self.assertEqual(c, 7)

    def test_cycle_length_100(self):
        c = collatz_cycle_length(100)
        self.assertEqual(c, 26)

    # ----
    # range_split
    # ----

    def test_range_split_1(self):
        low_ceiling, high_floor, cached = range_split(1, 2000)
        self.assertEqual(low_ceiling, 1)
        self.assertEqual(high_floor, 2000)
        self.assertEqual(cached, [179, 182])

    def test_range_split_2(self):
        low_ceiling, high_floor, cached = range_split(21, 2000)
        self.assertEqual(low_ceiling, 999)
        self.assertEqual(high_floor, 2000)
        self.assertEqual(cached, [182])

    def test_range_split_3(self):
        low_ceiling, high_floor, cached = range_split(1000, 2000)
        self.assertEqual(low_ceiling, 999)
        self.assertEqual(high_floor, 2000)
        self.assertEqual(cached, [182])

    def test_range_split_4(self):
        low_ceiling, high_floor, cached = range_split(1000, 2003)
        self.assertEqual(low_ceiling, 999)
        self.assertEqual(high_floor, 2000)
        self.assertEqual(cached, [182])

    def test_range_split_5(self):
        low_ceiling, high_floor, cached = range_split(999, 2000)
        self.assertEqual(low_ceiling, 999)
        self.assertEqual(high_floor, 2000)
        self.assertEqual(cached, [182])

    def test_range_split_6(self):
        low_ceiling, high_floor, cached = range_split(1, 999)
        self.assertEqual(low_ceiling, 1)
        self.assertEqual(high_floor, 999)
        self.assertEqual(cached, [179])

    def test_range_split_7(self):
        low_ceiling, high_floor, cached = range_split(1001, 3003)
        self.assertEqual(low_ceiling, 1999)
        self.assertEqual(high_floor, 3000)
        self.assertEqual(cached, [217])
    
    def test_range_split_8(self):
        low_ceiling, high_floor, cached = range_split(9000, 14000)
        self.assertEqual(low_ceiling, 8999)
        self.assertEqual(high_floor, 14000)
        self.assertEqual(cached, [260, 268, 250, 263, 276])

    def test_range_split_9(self):
        low_ceiling, high_floor, cached = range_split(2000, 2999)
        self.assertEqual(low_ceiling, 1999)
        self.assertEqual(high_floor, 2999)
        self.assertEqual(cached, [217])
    
    def test_range_split_10(self):
        low_ceiling, high_floor, cached = range_split(2000, 4999)
        self.assertEqual(low_ceiling, 1999)
        self.assertEqual(high_floor, 4999)
        self.assertEqual(cached, [217, 238, 215])

    def test_range_split_11(self):
        low_ceiling, high_floor, cached = range_split(1, 999)
        self.assertEqual(low_ceiling, 1)
        self.assertEqual(high_floor, 999)
        self.assertEqual(cached, [179])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(10,10)
        self.assertEqual(v, 7)

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 991, 0.0, "sing")
        self.assertEqual(w.getvalue(), "991 0.0 sing\n")
    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 100, 200, 125)
        self.assertEqual(w.getvalue(), "100 200 125\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n")

    def test_solve_2(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_3(self):
        r = StringIO("100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "100 200 125\n201 210 89\n900 1000 174\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
